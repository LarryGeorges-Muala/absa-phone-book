import datetime
from django.db import models

class PhoneBookUsers(models.Model):
    ''' Phone Book Users '''
    user_name = models.CharField(max_length=200, null=True, blank=True, default='')

    def secure_time_under_timezone():
        return datetime.datetime.now(datetime.timezone.utc)
    creation_date = models.DateTimeField('Creation Date', default=secure_time_under_timezone)

    def __str__(self):
        return self.user_name
    

class PhoneBookContacts(models.Model):
    ''' Contacts Linked To Phone Book Users '''
    user_reference = models.ForeignKey(PhoneBookUsers, on_delete=models.CASCADE)
    contact_name = models.CharField(max_length=200, null=True, blank=True, default='')
    contact_number = models.CharField(max_length=200, null=True, blank=True, default='')

    def secure_time_under_timezone():
        return datetime.datetime.now(datetime.timezone.utc)
    creation_date = models.DateTimeField('Creation Date', default=secure_time_under_timezone)

    def __str__(self):
        return '{} - {}'.format(self.contact_name, self.contact_number)
