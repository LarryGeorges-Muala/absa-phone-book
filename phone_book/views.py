from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponseRedirect, HttpRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
try:
	from django.urls import reverse
except:
	from django.core.urlresolvers import reverse
from django.utils import timezone
from phone_book import models


def create_sample_phone_books():
	models.PhoneBookUsers.objects.create(
		user_name='Public'
	)
	models.PhoneBookUsers.objects.create(
		user_name='Private'
	)
	return True


def save_verified_id(request, user_id):
	''' Save Verified ID '''
	try:
		if request.session.has_key('user_id'):
			del request.session['user_id']
		request.session['user_id'] = int(user_id)
		return True
	except Exception as error:
		print(error)
	return False


@csrf_exempt
def index(request):
	# create_sample_phone_books()

	if request.method == "POST":
		''' Create New Phone Book '''
		passed_name = request.POST.get('user-name', '')
		new_user = models.PhoneBookUsers.objects.create(
			user_name=passed_name
		)
		save_verified_id(request, new_user.id)
		return HttpResponseRedirect(reverse('phone_book:index'))

	''' Check Existing Users '''
	users = models.PhoneBookUsers.objects.all()
	users_list = []

	for single_user in users.order_by('user_name'):
		temp_obj = {
			'id': single_user.id,
			'name': single_user.user_name
		}
		users_list.append(temp_obj)

	context = {
		'users': users_list
	}
	return render(request, 'phone_book/index.html', context)


@csrf_exempt
def contacts(request):

	if request.method == "POST":
		''' User Details '''
		user_id = request.POST.get('user-id', '')
		
		''' Verify User '''
		user_id = int(user_id)
		user_check = models.PhoneBookUsers.objects.filter(id=user_id)

		if user_check:
			user_check = user_check.order_by('id').first()
		else:
			''' Log Error '''
			return HttpResponseRedirect(reverse('phone_book:index'))

		''' Save Verified ID '''
		try:
			if request.session.has_key('user_id'):
				del request.session['user_id']
			request.session['user_id'] = int(user_id)
		except Exception as error:
			print(error)
		return HttpResponseRedirect(reverse('phone_book:contacts'))

	contacts_list = []
	phone_book_user = ''

	''' Check Logged In User '''
	if request.session.has_key('user_id'):
		saved_user_id = request.session['user_id']

		''' Get User Contact List '''
		saved_user_id = int(saved_user_id)
		user_check = models.PhoneBookUsers.objects.filter(id=saved_user_id)

		if user_check:
			user_check = user_check.order_by('id').first()

			phone_book_user = {
				'id': user_check.id,
				'name': user_check.user_name
			}

			contact_list_check = models.PhoneBookContacts.objects.filter(
				user_reference=user_check
			)
			if contact_list_check:
				for contact in contact_list_check.order_by('contact_name'):
					temp_obj = {
						'id': contact.id,
						'name': contact.contact_name,
						'number': contact.contact_number
					}
					contacts_list.append(temp_obj)
	else:
		return HttpResponseRedirect(reverse('phone_book:index'))

	context = {
		'user': phone_book_user,
		'contacts_list': contacts_list
	}
	return render(request, 'phone_book/contacts.html', context)


@csrf_exempt
def edit_contacts(request):
	if request.method == "POST":
		''' contact details '''
		contact_id = request.POST.get('contact-edit-id', '')
		contact_name = request.POST.get('contact-edit-name', '')
		contact_number = request.POST.get('contact-edit-number', '')

		try:
			''' Get Contact '''
			target_contact = models.PhoneBookContacts.objects.filter(
				id=int(contact_id)
			)
			if target_contact:
				target_contact = target_contact.order_by('id').first()

				''' Update Contact '''
				target_contact.contact_name = contact_name
				target_contact.contact_number = contact_number
				target_contact.save()
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def create_contacts(request):
	if request.method == "POST":
		''' contact details '''
		phone_book_user_id = request.POST.get('new-contact-phone-book-id', '')
		new_contact_name = request.POST.get('new-contact-name', '')
		new_contact_number = request.POST.get('new-contact-number', '')

		try:
			''' Get Phone Book '''
			phone_book_user = models.PhoneBookUsers.objects.filter(
				id=int(phone_book_user_id)
			)
			if phone_book_user:
				phone_book_user = phone_book_user.order_by('id').first()

				''' Create Contact '''
				target_contact = models.PhoneBookContacts.objects.create(
					user_reference=phone_book_user,
					contact_name=new_contact_name,
					contact_number=new_contact_number
				)
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def delete_contacts(request):
	if request.method == "POST":
		''' contact details '''
		contact_id = request.POST.get('contact-delete-id', '')

		try:
			''' Get Contact '''
			target_contact = models.PhoneBookContacts.objects.filter(
				id=int(contact_id)
			)
			if target_contact:
				target_contact = target_contact.order_by('id').first()

				''' Delete Contact '''
				target_contact.delete()
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def edit_user_phone_book(request):
	if request.method == "POST":
		''' Phone Book Details '''
		phone_book_id = request.POST.get('phone-book-id', '')
		edited_phone_book_name = request.POST.get('phone-book-name', '')

		try:
			''' Get Phone Book '''
			target_phone_book = models.PhoneBookUsers.objects.filter(
				id=int(phone_book_id)
			)
			if target_phone_book:
				target_phone_book = target_phone_book.order_by('id').first()

				''' Update Phone Book '''
				target_phone_book.user_name = edited_phone_book_name
				target_phone_book.save()
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:contacts'))


@csrf_exempt
def delete_user_phone_book(request):
	if request.method == "POST":
		''' contact details '''
		phone_book_id = request.POST.get('phone-book-delete-id', '')

		try:
			''' Get Phone Book '''
			target_phone_book = models.PhoneBookUsers.objects.filter(
				id=int(phone_book_id)
			)
			if target_phone_book:
				target_phone_book = target_phone_book.order_by('id').first()

				''' Delete Phone Book '''
				target_phone_book.delete()
		except Exception as error:
			print(error)

	return HttpResponseRedirect(reverse('phone_book:index'))


def test_database_tables():
	try:
		phone_book_test = models.PhoneBookUsers.objects.create(
			user_name='Public'
		)

		if phone_book_test:
			test_contact = models.PhoneBookContacts.objects.create(
				user_reference=phone_book_test,
				contact_name='Test',
				contact_number='0000000000'
			)
			if test_contact:
				test_contact.delete()
			phone_book_test.delete()
			return True
	except Exception as error:
		print(error)
	return False
